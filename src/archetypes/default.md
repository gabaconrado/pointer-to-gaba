---
title: "{{ replace .Name "-" " " | title }}"
author: "Gabriel Conrado"
date: {{ .Date }}
draft: true
summary: ""
tags: []
categories: []
resources:
- name: "featured-image"
  src: "featured-image.jpg"
---

