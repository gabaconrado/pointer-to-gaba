---
title: "Welcome!"
author: "Gabriel Conrado"
date: 2023-11-11T12:00:42-05:00
draft: false
summary: "Welcome to my blog!"
tags: ["about"]
categories: ["presentation"]
resources:
- name: "featured-image"
  src: "featured-image.jpg"
---

Welcome to my personal blog, **Pointer to Gaba**. This blog will contain a bit of
everything that goes through my head, probably focused on programming, but there
can be some posts about games, animals and life in general lying around.

## Who am I? {#welcome}

My name is **Gabriel Conrado**, I'm a Brazilian software developer around his 30
years and I started to code a long time ago, when a software called [RPG Maker 2000](https://store.steampowered.com/app/383730/RPG_Maker_2000/)
promised to make very easy the task of creating your own game, and it actually was.

After this adventure, I got a technician degree and years later I graduated in
Computer Engineering, entering the IT market right away, which is what pay my bills
since then :).

The technologies I'm most familiar with are:

- C;
- Python;
- Rust;
- General automation (Docker, bash, CI);

Other personal interests are:

- Cats;
- Chess;
- Starcraft;

## Contact {#contact}

This blog is open source and is hosted [here](https://gitlab.com/gabaconrado/pointer-to-gaba).

Any question or suggestion can be mailed to me [here](mailto:gabaconrado@gmail.com).

And that's it :), welcome and let's code!
