---
title: "[00|00] Introduction"
author: "Gabriel Conrado"
date: 2022-01-13T09:00:02-03:00
draft: true
summary: "Introduction to my 'welcome to programming, you suck!' blog series"
tags: ["programming"]
categories: ["welcome-to-programming"]
resources:
- name: "featured-image"
  src: "featured-image.jpg"
---

Hello again! This is the beginning of my first series of posts: **Welcome to programming, you suck!**

The name is based on a nice [Dota2 beginner's guide](https://www.youtube.com/watch?v=yUcVxADBNeo),
and it is a joke on the fact that the game is very complex and overwhelming for new players.

Well, programming is not much different from that, learning to program may feel hard and frustrating
in the beginning (and even years after that sometimes), but is very rewarding and fun when things
start to click and when you see the results.

## Motivation {#motivation}

I code for good 10+ years now, and during this time the internet was my best friend and teacher,
the amount of information that you can find on it is insane. And together with the [Open-source](https://en.wikipedia.org/wiki/Open-source_software)
software idea, you can learn and evolve amazingly without having ever to pay for it.

Mostly of what I've used to learn and develop personal and professional projects is based in open
source software, which means that I'm using a lot from the community, and I feel like it is time to
start giving back something.

I want to do my best to try and pass a bit of what I know, but I'm not very used with writing or
creating content in general, so I'm open to any kind of feedback! You can reach me at **gabaconrado@gmail.com**.

## Target {#target}

The target of this series are people that knows what programming is, but never done anything related
to it. Knowing the basics of computers will be required since installation of tools and operational
systems will not be covered, although some relevant documentation will be linked when useful.

## Main goal {#goals}

The goal of this series is to give a general overview of what is needed to program, how a program is
developed and ran, and it will go through the process of developing small personal projects.

It will go from memory layout and compilers to code structure and _"good practices"_, it will cover
3 different programming languages to show how they are different and how to leverage their characteristics.

## Content {#content}

More specifically, the content will follow the topics below:

### Chapter 0 - Introduction

- What is programming, how computers work;
- Setting up a development environment;

### Chapter 1 - Let's start! - **Python** {#chapter-1}

- The interpreter, scripting languages and the community packages ecosystem;
- Python overview: Basic syntax and code structure, hello world example;
- Variables, the standard library, reading data from the user;
- Loops, conditionals, expressions, etc...;
- Functions and classes;
- Data structures;
- Public libraries: how to integrate them;
- Project structure, importing modules;
- Testing: Why are tests so important, how to test in python with _pytest_;
- Challenge: Chapter 1 project;

### Chapter 2 - Getting weird - **C** {#chapter-2}

- C overview: compilers, hello world example;
- Memory overview, the heap and the stack;
- Code structure: abstractions;
- Pointers;
- Challenge: Chapter 2 project;

### Chapter 3 - What the *** - **Rust** {#chapter-3}

- Rust overview: Basic syntax and unique characteristics;
- Resource ownership, moves and borrows;
- Processes and threads;
- Types and traits: Strongly defining software constraints;
- Errors and panics;
- Challenge: Chapter 3 project;

## Conclusion {#conclusion}

This will take a while to be written, but at the end I think it will be a good place
for people to understand a little more of the life of programmers :).

See you soon!
