---
title: "[00|01] Introduction to programming"
author: "Gabriel Conrado"
date: 2022-01-20T09:44:35-03:00
draft: true
summary: "Introduction to programming"
tags: ["programming", "introduction"]
categories: ["welcome-to-programming"]
resources:
- name: "featured-image"
  src: "featured-image.jpg"
---

Hello again! In this blog post we are going through the very basics of programming, in summary:

- What is a computer program;
- How a human-readable written code is understood by the computer;
- How the process of programming works, the human side;
- Algorithms;

## Computer programs {#programs}

The definition of a computer program, according to the [Wikipedia](https://en.wikipedia.org/wiki/Computer_program), is:

> "In imperative programming, a computer program is a sequence of instructions in a programming
> language that a computer can execute or interpret."

In a more informal definition, a very good friend of mine loves this other quote that I don't know
the origin:

> "A computer program is a set of distilled thoughts."

All of those are nice and answers the question by the human point-of-view, but if we think in the
computer point of view, the answer is quite simpler.

> **"A computer program is a (very) long string of 0s and 1s."**

&nbsp;&nbsp;![Software representations](sw-representation.en.png "Software representations")&nbsp;&nbsp;

The idea is simple, but the execution is very complex, since not any random array of 0s and 1s will
yield a valid computer program. These _bits_ (as the 0s and 1s are called) must be carefully organized
and ordered in a way that your computer hardware will understand it and do what you want it to do.

One thing to have in mind is, software is done using other software. This means that the software
development ecosystem is formed by many layers of different systems that works together.

### Compilers {#compilers}

The compiler is our translator in this quest! It is the software responsible for taking a human-readable
file as an input and spitting out a computer-readable binary. Every compiler will be specific to a
**programming language**.

> "A programming language is any set of rules that converts strings to various kinds of machine code outputs"

&nbsp;&nbsp;![Compiler process](compiler-process.en.png "Compiler representation")&nbsp;&nbsp;

If your input file, also known as **source code**, follows correctly the rules of the language, the
compiler will output a valid binary file ready to be run.

### Programming {#programming}

So, all of this looks very simple, just follow a couple of rules, and you'll be able to launch your
revolutionary amazing Operational System™, right!?

Sure, there is one thing though, **the computer is pretty dumb**, no offense for all the computers out there.

The hardest part about programming is making sure that the computer understands what you want
it to do. You have to cover every possible scenario, every possible input and deal with all the
nuances related to the domain of your application. If something happens that the computer does not
understand or isn't expecting, it will simply halt, or worse, act unpredictably.

This is where we define **algorithms**:

> "In mathematics and computer science, an algorithm is a finite sequence of well-defined instructions,
> typically used to solve a class of specific problems or to perform a computation."

Using algorithms, we can systematically provide a set of detailed instructions with a well known
and predictable execution flow.

### Algorithms {#algorithms}

The idea of an algorithm is to provide complete instructions to perform a specific task.

It is important that there are no ambiguities in any of the instructions, since ambiguities will end up
being solved by the entity performing the algorithm. For humans this is not generally a problem since
humans have a pretty good mental model to understand what was meant by that instruction, but machines
will have issues.

For instance, an algorithm to prepare a milkshake, that a human can follow, will look like:

```text
- Put 2 scoops of vanilla ice-cream;
- Put 2 scoops of chocolate ice-cream;
- Put 4 scoops of milk;
- Mix it up;
- Serve!
```

Looks like a roughly complete algorithm, since it says exactly all the ingredients of the milkshake,
however, it contains a lot of implicit assumptions that may be obvious to humans, for instance:

- You have to have the ice-cream recipients close to you;
- You have to have the milkshake recipient close to you;
- You have to open/close the ice-cream recipients, you have to place everything together in the blender;
- The blender should be plugged in a power supply;
- Etc...

They are not at all obvious for a generic computer, though. If we gave this algorithm to a simple
robot, the behavior would be quite unpredictable, and our results may be very different from a
delicious milkshake.

With that in mind, when writing, we can go as far as we want in the level of details that we need to
perform even a simple task, and how much detailed we need to be will depend on the environment that
will run our algorithm.

If it is a well configured specific milkshake maker robot, maybe it will understand the simplified
version, but if we're using a more generic environment, we have to be more detailed.

For instance:

```text
- Get 2 scoops, 1 milkshake cup, 1 box of milk and 1 blender from the cabinets and put it on the counter;
- Get the vanilla ice-cream and the chocolate ice-cream from the freezer and put it on the counter;
- Assemble the blender, plug it in a power socket;
- Open the vanilla ice-cream recipient, fill 1 scoop with it, throw it in the blender, repeat once;
- Close and put away the vanilla ice-cream recipient back in the freezer;
- Open the chocolate ice-cream recipient, fill 1 scoop with it, throw it in the blender, repeat once;
- Close and put away the chocolate ice-cream recipient back in the freezer;
- Open the milk box, fill 1 scoop with it, throw it in the blender, repeat 3 times;
- Close and put away the milk box back in the cabinet;
- Turn on the blender, wait for 15 seconds, turn off the blender;
- Put the mixed liquid from the blender into the milkshake cup slowly;
- Serve the cup to the person that requested it;
```

And we could go even further, but I'm going to end with this one for now.

### Conclusion {#conclusion}

When we combine the syntax rules of a programming language with the semantics from a well written
algorithm, we then achieve the software we want!

Thanks for staying with me until here, and go treat yourself with a nice milkshake!

As always, if you have any feedback or questions, reach me at **gabaconrado@gmail.com**.

Bye!
