---
title: "[00|02] Basic Development Environment"
author: "Gabriel Conrado"
date: 2022-03-21T07:33:05-03:00
draft: true
summary: "A few pointers to achieve a development minimal setup"
tags: ["dev-tools", "environment", "setup"]
categories: ["welcome-to-programming"]
resources:
- name: "featured-image"
  src: "featured-image.jpg"
---

<!-- LTeX: language=en -->

Setup environment
Hello again! In this post we are going through the process of creating a development environment.

## Motivation {#motivation}

There are plenty of good resources out there on how to install and setup good tools. Also, these
kind of guides usually become outdated very quick.

The idea of this post, thus, is to give basic knowledge and a few pointers on how to have a
basic environment to start.

## Goal {#goal}

Our goal is to have an environment where we can write our code, compile it and run it in an easy way.

## The easiest way: Online tools {#online-tools}

The easiest and faster way to write code and see it working is using online servers that will provide
a text editor and run our code in their own setups. These environments are usually named playgrounds.

A very cool one is [Glot][7]. It has a lot of languages and the interface is easy and fast. I'd
recommend to use it until you get the basics, and then move to use your own tools.

## The hard (but better) way: Local tools {#local-tools}

Using local tools will boost a lot your productivity and will naturally feel more comfortable when
you get used with what you like. However, if you never used anything, it may take a time (and a lot
of tries) until you find a good setup.

The chosen tools here are a suggestion, feel free to try other stuff and use them if you prefer.
The setup I'm gonna suggest is the one I use.

### Operational System {#os}

First thing is understanding how to work in the operational system you use.

For Linux or Mac users, you won't need much setup, since you're probably familiar with the command line.

> In this blog I'll always be using the command line (terminal) to run commands.

For Windows users, I strongly suggest the usage of _Windows Subsystem for Linux_(WSL). Along with with the
new _Windows Terminal_.
It is a great way of having the Linux tools and simplicity, without having to actually use it full time.

If you never used Linux before, Ubuntu is a great distribution to start! It is the default distribution
installed when you install the WSL.

To install WSL, follow the instructions from the [official website][1].

With your OS defined and installed, it is very useful to have an understanding of the command line (shell).
There is a very nice [MIT class][2] that can you a good overview of what it is and how to start using it.

### Text Editor or IDE? {#txt-ide}

When coding you will spend most of your time writing text. For this reason, it is important to have a
comfortable way of editing and navigating text.

One option is to use a text editor, which is a more generic software that will work for anything
that you want to write. This means that the setup/compiling/running part of software development is
on you.

The other option is to use an Integrated Development Environment. An IDE is a software that will
provide more than editing text capabilities, it will take care of the whole process for you.
The downside of it is that IDEs are language-specific. This means that you would have to know how
to work with many IDEs if you want to work with many languages.

Both options are good, and there are modern text editors that can be configured to have most of the
capabilities of IDEs (if you put some effort into learning how to configure them).

I suggest the use of the [Visual Studio Code][3] text editor.

#### Visual Studio Code

VS Code is a very popular text editor. It is very intuitive and powerful. Its core strength is in the plugin
ecosystem that makes possible for you to customize it and configure it for your specific needs.

VS Code also integrates nicely with the WSL, making it possible to develop inside your Linux environment
directly from Windows.

### Compilers/Interpreters

In this blog series we will be using C, Python and Rust.

To process and run our code, we need to have the tools needed installed.

- For Python, we will use [Python 3][4];
- For C, we will use [gcc][5];
- For Rust, we will use the [Rust compiler][6];

## Conclusion

Being comfortable with your tools is essential for software development. The suggestions in this
post work for me but it might not work for you. Feel free to test whatever feels better :).

In the next post we will start our long journey in software development with the basics of Python!

As always, if you have any feedback or questions, reach me at **gabaconrado@gmail.com**.

Bye!

[1]: https://docs.microsoft.com/en-us/windows/wsl/install
[2]: https://missing.csail.mit.edu/2020/course-shell/
[3]: https://code.visualstudio.com/
[4]: https://www.python.org
[5]: https://gcc.gnu.org/
[6]: https://www.rust-lang.org/
[7]: https://glot.io
